extends Area2D


export (PackedScene) var Bullet
export (PackedScene) var Explosion
export var base_speed = 400
var speed = base_speed
var screen_size
export var armor = 2 setget set_armor
export var extra_armor = 0 setget set_extra
signal hit
signal dead
signal extra


# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size
	hide()


func setup(level):
	$ShootTimer.wait_time = float(1/level)


func _process(delta):
	var velocity = Vector2()
	if Input.is_action_pressed("ui_slow"):
		speed = base_speed/2
	else:
		speed = base_speed
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
	if Input.is_action_pressed("ui_up"):
		velocity.y -= 1
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed

	position += velocity * delta
	position.x = clamp(position.x, 0, screen_size.x)
	position.y = clamp(position.y, 0, screen_size.y)


func decrement_armor():
	if extra_armor > 0:
		extra_armor -= 1
		emit_signal("hit")
		create_explosion()
	else:
		set_armor(armor-1)


func set_armor(value):
	armor = value
	emit_signal("hit")
	create_explosion()
	if armor <= 0:
		emit_signal("dead")
		$ShootTimer.stop()
		hide()
		$CollisionShape2D.set_deferred("disabled", true)
		#queue_free()
		

func set_extra(value):
	extra_armor = value
	emit_signal("extra")


func start(pos):
	position = pos
	show()
	$ShootTimer.start()
	$CollisionShape2D.disabled = false


func create_bullet(pos, isback):
	var bullet = Bullet.instance()
	bullet.position = pos
	if isback:
		bullet.velocity = Vector2(0, 500)
	get_tree().get_root().add_child(bullet)


func shoot():
	var left = $Cannons/Left.get_global_position()
	var right = $Cannons/Right.get_global_position()
	var back = $Cannons/Back.get_global_position()
	create_bullet(left,false)
	create_bullet(right, false)
	create_bullet(back, true)


func create_explosion():
	var explosion = Explosion.instance()
	explosion.position = get_global_position()
	get_tree().get_root().add_child(explosion)


func _on_ShootTimer_timeout():
	shoot()
