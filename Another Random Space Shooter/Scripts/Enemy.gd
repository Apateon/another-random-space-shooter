extends Area2D


export var velocity = Vector2(0, 50)
export var level = 1
export var armor = 2 setget set_armor
export (PackedScene) var Bullet_enemy
export (PackedScene) var Destroyed
signal dead


# Called when the node enters the scene tree for the first time.
func setup(level):
	$ShootTimer.wait_time = (float(4)/float(level))
	armor = level


func _ready():
	set_process(true)
	$ShootTimer.start()


func _process(delta):
	translate(velocity * delta)
	
	
func destroyed_anim():
	var ex = Destroyed.instance()
	ex.position = get_global_position()
	get_tree().get_root().add_child(ex)


func create_bullet(pos, direction):
	var bullet = Bullet_enemy.instance()
	bullet.position = pos
	bullet.rotation = direction
	bullet.velocity = bullet.velocity.rotated(direction)
	get_tree().get_root().add_child(bullet)


func shoot(isleft):
	var direction = rotation
	if isleft:
		create_bullet($Cannons/Left.get_global_position(), (direction + (PI/2)))
	else:
		create_bullet($Cannons/Right.get_global_position(), (direction - (PI/2)))


func _on_ShootTimer_timeout():
	shoot(true)
	shoot(false)


func set_armor(value):
	armor = value
	if armor <= 0:
		destroyed_anim()
		emit_signal("dead") 
		queue_free()


func set_level(val):
	level = val


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()


func _on_Enemy_area_entered(area):
	if area.is_in_group("player"):
		area.decrement_armor()
		queue_free()
