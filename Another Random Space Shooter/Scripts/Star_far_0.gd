extends Sprite


export var velocity = Vector2(0,50)  # defines variable "velocity" and exposes it in inspector (for control)

func _ready():
	set_process(true)
	pass

func _process(delta):
	translate(velocity * delta)  # translates sprite by velocity multiplied by change per frame draw (time)
	var pos = get_position().y  # define variable "pos" as current position of sprite
	var pos_start = Vector2(0,-480)  # defines variable for start point (-180px on y)
	var pos_end = get_viewport_rect().size.y  # defines variable for end point (end of viewport)
 
	if pos >= pos_end:
		set_position(pos_start)  # self explanatory
	pass
