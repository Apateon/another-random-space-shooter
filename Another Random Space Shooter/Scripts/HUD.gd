extends CanvasLayer
signal start_game
signal next_level


var on_next = false


# Called when the node enters the scene tree for the first time.
func _ready():
	$NextButton.hide()
	$NextMessage.hide()
	
	
func _process(delta):
	if on_next:
		if Input.is_action_just_pressed("ui_accept"):
			$NextButton.hide()
			$NextMessage.hide()
			emit_signal("next_level")
			on_next = false


func show_message(text):
	$Message.text = text
	$Message.show()
	$MessageTimer.start()


func show_game_over():
	show_message("Game Over")
	# Wait until the MessageTimer has counted down.
	yield($MessageTimer, "timeout")
	
	$Message.text = "Stay Alive!"
	$Message.show()
	# Make a one-shot timer and wait for it to finish.
	yield(get_tree().create_timer(1), "timeout")
	$StartButton.show()


func show_next_level():
	show_message("Next Level")
	yield($MessageTimer, "timeout")
	
	$Message.text = "Stay Alive!"
	$Message.show()
	yield(get_tree().create_timer(1), "timeout")
	#$NextButton.show()
	$NextMessage.show()
	on_next = true
	


func update_score(score):
	$ScoreLabel.text = str(score)


func update_nuke(armor):
	$NukeLabel.text = str("Nuke: ", (armor-1))


func update_level(level):
	$LevelLabel.text = str("Level: ", level)
	

func update_kill(kill):
	$KillLabel.text = str("Kill: ", kill)


func _on_StartButton_pressed():
	$StartButton.hide()
	emit_signal("start_game")

func _on_MessageTimer_timeout():
	$Message.hide()


func _on_NextButton_pressed():
	$NextButton.hide()
	emit_signal("next_level")
