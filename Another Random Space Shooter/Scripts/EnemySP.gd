extends Area2D


export var velocity = Vector2(0, 50)
export var level = 1
export var armor = 2 setget set_armor
export (PackedScene) var Extra_live
export (PackedScene) var Destroyed
signal dead


# Called when the node enters the scene tree for the first time.
func setup(level):
	armor = level


func _ready():
	set_process(true)


func _process(delta):
	translate(velocity * delta)
	
	
func destroyed_anim():
	var ex = Destroyed.instance()
	ex.position = get_global_position()
	get_tree().get_root().add_child(ex)


func create_live(pos):
	var live = Extra_live.instance()
	live.position = pos
	get_tree().get_root().add_child(live)


func set_armor(value):
	armor = value
	if armor <= 0:
		destroyed_anim()
		create_live(get_global_position())
		emit_signal("dead") 
		queue_free()


func set_level(val):
	level = val


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()


func _on_EnemySP_area_entered(area):
	if area.is_in_group("player"):
		area.decrement_armor()
		queue_free()
