extends Node


export (PackedScene) var Enemy
export (PackedScene) var EnemySP
export var level = 1
var score
var kill


func _ready():
	randomize()


func new_game():
	start(1)
	kill = 0
	$HUD.update_kill(kill)
	$Ship.start($StartPosition.position)


func game_over():
	$ScoreTimer.stop()
	$MobTimer.stop()
	$HUD.show_game_over()
	$Ship.armor=2
	get_tree().call_group("enemy", "queue_free")
	get_tree().call_group("enemy_bullet", "queue_free")


func end_level():
	$ScoreTimer.stop()
	$Ship.armor=2
	$MobTimer.stop()
	$HUD.show_next_level()
	get_tree().call_group("enemy", "queue_free")
	get_tree().call_group("enemy_bullet", "queue_free")


func next_level():
	start(level+1)


func start(val):
	level = val
	score = level*10
	$HUD.update_score(score)
	$HUD.update_level(level)
	$HUD.update_nuke($Ship.armor+$Ship.extra_armor)
	$HUD.show_message("Get Ready\nHold shift\nto slow down")
	$MobTimer.wait_time=float(3)/float(level)
	$StartTimer.start()
	setup()


func setup():
	$Ship/ShootTimer.wait_time = float(1)/float(level)
	

func spawn(enemy):
	# Choose a random location on Path2D.
	$EnemyPath/EnemySpawnLocation.offset = randi()
	# Create a Mob instance and add it to the scene.
	var mob = enemy.instance()
	mob.setup(level)
	add_child(mob)
	mob.connect('dead', self, '_on_Enemy_dead')
	# Set the mob's direction perpendicular to the path direction.
	var direction = $EnemyPath/EnemySpawnLocation.rotation
	# Set the mob's position to a random location.
	mob.position = $EnemyPath/EnemySpawnLocation.position
	# Add some randomness to the direction.
	direction += rand_range(-PI / 4, PI / 4)
	mob.rotation = direction
	# Set the velocity (speed & direction).
	mob.velocity = mob.velocity.rotated(direction)


func _on_StartTimer_timeout():
	$MobTimer.start()
	$ScoreTimer.start()


func _on_ScoreTimer_timeout():
	score -= 1
	$HUD.update_score(score)
	if score == level * 5:
		spawn(EnemySP)
	if score <= 0:
		if level == 5:
			game_over()
		else: end_level()


func _on_MobTimer_timeout():
	spawn(Enemy)


func _on_Ship_hit():
	get_tree().call_group("enemy", "queue_free")
	get_tree().call_group("enemy_bullet", "queue_free")
	$HUD.update_nuke($Ship.armor+$Ship.extra_armor)
	
	
func _on_Enemy_dead():
	kill += 1
	$HUD.update_kill(kill)


func _on_Ship_extra():
	$HUD.update_nuke($Ship.armor+$Ship.extra_armor)
